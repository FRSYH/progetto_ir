from __future__ import absolute_import, division, print_function, unicode_literals
import functools

import sklearn
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling
from sklearn.model_selection import train_test_split
import seaborn as sb
import matplotlib.pyplot as plt
from tensorflow.keras import regularizers
import imblearn
from imblearn.over_sampling import RandomOverSampler
from imblearn.over_sampling import SMOTE
from imblearn.over_sampling import ADASYN
from mpl_toolkits.mplot3d import axes3d
from matplotlib import style
from imdb import IMDb
import ast
from ast import literal_eval
from sklearn.model_selection import StratifiedShuffleSplit
from collections import Counter

style.use('ggplot')

print(tf.__version__)

'''
rating prediction.py
legge un dataset già trattato con le metriche adeguate
a seconda della rete scelta si costruisce il modello e si addestra la rete
i dati vengono tratti diversamente a seconda delle reti (formati diversi richiesti in input e output)
anche il display delle statistiche varia a seconda della rete/formato dei dati
'''


# ----- Functions ---------
# se la rete usa softmax si converte il rating in un valore intero per poter essere classificato correttamente
def extract_label(df):
    df = df.reset_index(drop=True)
    rating = df.pop('rating')
    if network_type == 'classification' or network_type == 'convolutional':
        rating = rating.apply(lambda x: round(x))
    return df, rating


def plot_prediction_truevalues(test_x, test_y):
    test_prediction = model.predict(np.asarray(test_x))
    plt.title('Testset predictions')
    plt.xlabel('test observation')
    plt.ylabel('imdb_rating')
    plt.plot(np.arange(test_prediction.size), test_y, 'r', label="expected value")
    plt.plot(np.arange(test_prediction.size), test_prediction, 'b', label="prediction")
    plt.legend(loc="upper right")
    plt.show()


def regression_model(train_x, train_y, test_x, test_y):
    model = keras.Sequential([
        keras.layers.Dense(300, activation='sigmoid', input_shape=(train_x.shape[1],)),
        keras.layers.Dropout(0.1),
        keras.layers.Dense(300, activation='sigmoid'),
        keras.layers.Dropout(0.1),
        keras.layers.Dense(1)
    ])
    print(model.summary())
    optimizer = keras.optimizers.RMSprop(learning_rate=0.0001, decay=1e-6)
    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae', 'mse'])
    model.fit(
        train_x.values, train_y.values,
        batch_size=32, epochs=50, validation_data=(test_x.values, test_y.values))
    return model


def classification_model(train_x, train_y, test_x, test_y):
    model = keras.Sequential([
        keras.layers.Dense(300, activation='sigmoid', input_shape=(train_x.shape[1],)),
        keras.layers.Dropout(0.1),
        keras.layers.Dense(300, activation='sigmoid'),
        keras.layers.Dropout(0.1),
        keras.layers.Dense(11, activation='softmax')
    ])
    print(model.summary())
    optimizer = keras.optimizers.Adam(learning_rate=0.0001)
    model.compile(optimizer=optimizer,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    model.fit(
        train_x.values, train_y.values,
        batch_size=100, epochs=150, validation_data=(test_x.values, test_y.values))
    return model


def convolutional_model(train_x, train_y, test_x, test_y):
    train_x = train_x.to_numpy()
    train_x = train_x.reshape((train_x.shape[0], train_x.shape[1], 1))
    test_x = test_x.to_numpy()
    test_x = test_x.reshape((test_x.shape[0], test_x.shape[1], 1))
    model = keras.Sequential([
        keras.layers.Conv1D(filters=100, kernel_size=10, activation='relu', input_shape=(train_x.shape[1], 1),
                            padding='same'),
        keras.layers.Dropout(0.5),
        keras.layers.Conv1D(filters=100, kernel_size=10, activation='relu', padding='same'),
        keras.layers.Dropout(0.5),
        keras.layers.Conv1D(filters=100, kernel_size=10, activation='relu', padding='same'),
        keras.layers.Dropout(0.5),
        keras.layers.Flatten(),
        keras.layers.Dense(11, activation='softmax')])

    print(model.summary())

    optimizer = keras.optimizers.Adam(learning_rate=0.001)
    model.compile(optimizer=optimizer,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    model.fit(
        train_x, train_y.values,
        batch_size=100, epochs=150, validation_data=(test_x, test_y.values))
    return model


def convolutional_regression_model(train_x, train_y, test_x, test_y):
    train_x = train_x.to_numpy()
    train_x = train_x.reshape((train_x.shape[0], train_x.shape[1], 1))

    test_x = test_x.to_numpy()
    test_x = test_x.reshape((test_x.shape[0], test_x.shape[1], 1))

    model = keras.Sequential([
        keras.layers.Conv1D(filters=100, kernel_size=10, activation='sigmoid', input_shape=(9, 1), padding='same'),
        keras.layers.Dropout(0.5),
        keras.layers.Conv1D(filters=100, kernel_size=10, activation='sigmoid', padding='same'),
        keras.layers.Dropout(0.5),
        keras.layers.Conv1D(filters=100, kernel_size=10, activation='sigmoid', padding='same'),
        keras.layers.Dropout(0.5),
        keras.layers.Flatten(),
        keras.layers.Dense(300, activation='sigmoid'),
        keras.layers.Dropout(0.5),
        keras.layers.Dense(1)])

    print(model.summary())

    optimizer = keras.optimizers.RMSprop(learning_rate=0.0001, decay=1e-6)
    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae', 'mse'])
    early_stopping = keras.callbacks.EarlyStopping(monitor='mse', patience=10)

    model.fit(
        train_x, train_y.values,
        batch_size=100, epochs=250, validation_data=(test_x, test_y.values))
    return model


def extract_test_label(test):
    test_x = []
    test_y = []
    for feature, label in test:
        test_x.append(feature.numpy())
        test_y.append(label.numpy())
    test_x = np.asarray(test_x)
    test_y = np.asarray(test_y)
    return test_x, test_y


def draw_correlation_heatmap(train, annotated=False):
    corrletaion = train.corr()
    ax = plt.gca()
    sb.heatmap(corrletaion, vmax=.8, square=True, cmap="YlGnBu", annot=annotated, linewidths=.3)
    plt.yticks(rotation=0)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha="right")
    plt.show()


def draw_correlation_heatmap_categoric(train, annotated=False):
    correlation = train.corr()
    print(correlation['rating'])
    correlation = train[['rating', 'directors', 'cast', 'production companies']].corr()
    sb.heatmap(correlation, vmax=.8, square=True, cmap="YlGnBu", annot=annotated, linewidths=.3)
    plt.yticks(rotation=0)
    plt.show()


def draw_error_distribution(test_prediction, test_y, ax):
    if network_type == 'regression' or network_type == 'convolutional_regression':
        error = test_prediction.flatten() - test_y.values
        max = int(error.max())
        min = int(error.min())
        min_max = (min, max)
        labels = list(range(min, max + 1))
        num_points = (max - min) * 2
        labels = np.linspace(min, max, num_points)
        ax.hist(error, bins=labels, range=min_max)
        labels = list(map(lambda point: round(point, 2), labels))
    else:
        error = test_prediction - test_y.values
        print(error)
        keys = Counter(error).keys()
        values = Counter(error).values()
        labels = list(range(error.min(), error.max() + 1))
        rects = ax.bar(keys, values)
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')
    ax.set_xlabel("Prediction Error")
    ax.set_ylabel("Count")
    ax.set_xticks(labels)
    if network_type == 'regression' or network_type == 'convolutional_regression':
        ax.set_xticklabels(labels, rotation=45, ha="right")
    else:
        ax.set_xticklabels(labels)


def draw_rating_distribution(df, column='rating'):
    if network_type == 'classification':
        fig, ax = plt.subplots()
        ax.bar(range(1, 11), dataset[column].map(lambda x: round(x)).value_counts().sort_index().tolist())
        ax.set_xticks(range(1, 11))
        ax.set_xticklabels(list(range(1, 11)))
        plt.show()
    else:
        plt.hist(df[column].values, bins=20)
        plt.xlabel("imdb_score")
        plt.ylabel("Count")
        plt.show()


def scatter_prediction_truevalues(test_truevalue, test_prediction, ax):
    ax.scatter(test_truevalue, test_prediction)
    ax.set_xlabel('True Values')
    ax.set_ylabel('Predictions')
    y_lims = [0, 11]
    x_lims = [0, 11]
    ax.set_xlim(y_lims)
    ax.set_ylim(x_lims)
    ax.plot(x_lims, y_lims)


def scatter_prediction_truevalues_annotated(test_truevalue, test_prediction, count, ax):
    for i, item in enumerate(count):
        x = test_truevalue[i]
        y = test_prediction[i]
        ax.scatter(x, y)
        ax.text(x + 0.1, y + 0.1, item, fontsize=9)
    ax.set_xlabel('True Values')
    ax.set_ylabel('Predictions')
    y_lims = [0, 11]
    x_lims = [0, 11]
    labels = list(range(0, 11))
    ax.set_xticks(labels)
    ax.set_yticks(labels)
    ax.set_xticklabels(labels)
    ax.set_xlim(y_lims)
    ax.set_ylim(x_lims)
    ax.plot(x_lims, y_lims)

# conta la numerosità dei punti predetti
def network_prediction(test_prediction, test_y):
    tuple_list = []
    flag = 0
    for index in range(len(test_y)):
        tuple_list.append((test_y[index], test_prediction[index]))
    tuple_set = set(tuple_list)

    tuple_dict = {}
    for t in tuple_set:
        tuple_count = 0
        for possible_t in tuple_list:
            if possible_t == t:
                tuple_count += 1
        tuple_dict[t] = tuple_count

    x = []
    y = []
    z = []
    for key in tuple_dict:
        z.append(tuple_dict[key])
        x.append(key[0])
        y.append(key[1])
    return x, y, z


def display_statistics(x_data, y_data, x=None, y=None, z=None):
    fig, axes = plt.subplots(ncols=2, figsize=(10, 8))
    fig.suptitle('Statistics of predicted values on validation data', fontsize=16)
    if network_type == 'classification' or network_type == 'convolutional':
        scatter_prediction_truevalues_annotated(x, y, z, axes[0])
    else:
        scatter_prediction_truevalues(y_data, x_data, axes[0])
    draw_error_distribution(x_data, y_data, axes[1])
    plt.show()


def plot_rating(dataset):
    rating = dataset.loc[:, 'rating']
    draw_rating_distribution(rating)


def draw_class_barplot(df):
    fig, ax = plt.subplots()
    ax.bar(range(1, 11), df['class'].value_counts().sort_index().tolist())
    ax.set_xticks(range(1, 11))
    ax.set_xticklabels(list(range(1, 11)))
    plt.show()


def drop_feature(df):
    dataset.pop('primary_title')
    dataset.pop('title_id')
    dataset.pop('genres')
    correlation = df.corr()['rating']
    for feature, feature_correlation in correlation.items():
        if feature_correlation < 0.1:
            dataset.pop(feature)


def balance_sampling(df):
    for index, row in df.iterrows():
        df.at[index, 'class'] = round(df.at[index, 'rating'])
    draw_class_barplot(df)
    threshold = 15000
    g = df.groupby('class')
    df = g.apply(lambda x: x.sample(threshold, replace=True) if x.shape[0] > threshold else x).reset_index(drop=True)
    draw_class_barplot(raw_train)
    df.pop('class')
    return df


# ----- MAIN -------------

network_type = 'convolutional'
metric = 'rating'

dataset = pd.read_csv(f'test_imdb_{metric}.csv', index_col=0)
dataset = dataset.dropna()
dataset = dataset.reset_index(drop=True)

drop_feature(dataset)
dataset = dataset.astype('float64')

print(dataset.describe())
print(dataset)

draw_rating_distribution(dataset)

if dataset.shape[1] < 15:
    draw_correlation_heatmap(dataset, annotated=True)
else:
    draw_correlation_heatmap_categoric(dataset, annotated=True)

# estrae trai e test set
raw_train, raw_test = train_test_split(dataset, test_size=0.2)
raw_train = raw_train.reset_index(drop=True)
raw_test = raw_test.reset_index(drop=True)

train_x, train_y = extract_label(raw_train)
test_x, test_y = extract_label(raw_test)

# genera ed addestra il modello
if network_type == 'classification':
    model = classification_model(train_x, train_y, test_x, test_y)
elif network_type == 'regression':
    model = regression_model(train_x, train_y, test_x, test_y)
elif network_type == 'convolutional':
    model = convolutional_model(train_x, train_y, test_x, test_y)
elif network_type == 'convolutional_regression':
    model = convolutional_regression_model(train_x, train_y, test_x, test_y)

if network_type == 'convolutional' or network_type == 'convolutional_regression':
    train_xx = train_x.to_numpy()
    train_xx = train_xx.reshape((train_x.shape[0], train_x.shape[1], 1))
    test_xx = test_x.to_numpy()
    test_xx = test_xx.reshape((test_x.shape[0], test_x.shape[1], 1))
else:
    test_xx = test_x

# valuta il modello sul test set
print('\nevaluate model on test set\n')
model.evaluate(test_xx, test_y)

# effettua predizioni sul test set per poterle visualizzare
test_prediction = model.predict(np.asarray(test_xx))

# converte il risultato softmax in un formato compatibile
if network_type == 'classification' or network_type == 'convolutional':
    predictions = []
    for prediction in test_prediction:
        predictions.append(np.argmax(prediction))
    test_prediction = predictions

# mostra le statistiche corrette per lo specifico tipo di rete
if network_type == 'classification' or network_type == 'convolutional':
    x, y, z = network_prediction(test_prediction, test_y)
    display_statistics(test_prediction, test_y, x, y, z)
else:
    display_statistics(test_prediction, test_y)
