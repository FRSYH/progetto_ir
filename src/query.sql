SELECT clean_title.title_id, primary_title, is_adult, premiered, runtime_minutes, genres, rating, votes
FROM (SELECT title_id, primary_title, is_adult, premiered, runtime_minutes, genres 
		FROM titles 
		WHERE type='movie' AND premiered NOTNULL AND runtime_minutes NOTNULL AND genres <> '\N') as clean_title
	 ,
	 (SELECT title_id, rating, votes FROM ratings) as clean_ratings
WHERE clean_title.title_id=clean_ratings.title_id
ORDER BY clean_title.premiered