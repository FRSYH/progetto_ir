import pandas as pd
import numpy as np
from imdb import IMDb, IMDbError
import concurrent.futures
from price_parser import Price
from itertools import repeat
from currency_converter import CurrencyConverter


'''
dataset_creator.py 

Recupera tramite imdbpy tutte le informazioni imdb di uno specifico film.
Partendo dalla lista di film pubblica di IMDB si richiedono le informazioni di ogni film.
La creazione avviene in chiunks per evitare problemi di connessione.
La creazione di un chunk avviene in maniera multithread per veloccizzare le richieste.
'''

def convert_currency(price):
    if price.currency == '$':
        currency = 'USD'
    elif price.currency == '€':
        currency = 'EUR'
    else:
        currency = price.currency
    if currency != 'USD':
        converted_price = converter.convert(price.amount, currency, 'USD')
        return int(converted_price)
    else:
        return int(price.amount_float)


def thread_function(df):
    for index, row in df.iterrows():
        year = row['premiered']
        if start <= year <= end:
            title = row['primary_title']
            imdb_id = row['title_id'][2:]
            movie = ia.get_movie(imdb_id, info=['main'])
            df.at[index, 'main'] = movie

    print('thread finished requests')

    # dopo aver finito le richieste si effettua del preprocessing sui dati, necessario per risparmiare spazio
    # la maggior parte dei dati raccolti non ci interessa, si recuperano solo le informazioni sottostanti
    features = ['directors', 'cast', 'production companies', 'box office']
    box_office_sub_feature = ['Budget', 'Cumulative Worldwide Gross']

    for index, row in df.iterrows():
        movie = row['main']
        for feature in features:
            if feature == 'box office':
                if feature in movie:
                    box_office = movie[feature]
                    for sub_feature in box_office_sub_feature:
                        if sub_feature in box_office:
                            df.at[index, sub_feature] = box_office[sub_feature]
            else:
                # per le feature complesse come cast si creano delle liste che contengono tutti gli elementi
                element_list = []
                if feature in movie:
                    for item in movie[feature]:
                        if feature == 'production companies':
                            element_list.append(item.companyID)
                        else:
                            element_list.append(item.personID)
                    df.at[index, feature] = element_list
    return df


ia = IMDb()
converter = CurrencyConverter()

# la divisione in anni non è omogenea, ci sono molti più film negli anni recenti
start = 1960
end = 1970
increment = 10
stop = 2020

# numero di thread limitato a 16, oltre IMDB blocca le richieste proveniente dallo stesso ip
num_threads = 16
# dataset di dati pubblici imdb che contiene gli id dei film da ricercare tramite imdbpy
dataset = pd.read_csv('../datasets/custom_imdb/imdb.csv')
print(dataset.dtypes)
dataset['main'] = ''
dataset['directors'] = ''
dataset['cast'] = ''
dataset['production companies'] = ''
dataset['Budget'] = ''
dataset['Cumulative Worldwide Gross'] = ''

while start < stop:
    print(f'start:{start} - end:{end}')
    df_copy = dataset.copy()
    index_names = df_copy[(df_copy['premiered'] < start) | (df_copy['premiered'] > end)].index
    df_copy.drop(index_names, inplace=True)
    df_copy = df_copy.reset_index()
    df_list = np.array_split(df_copy, num_threads)

    with concurrent.futures.ThreadPoolExecutor(max_workers=num_threads) as executor:
        executor.map(thread_function, df_list)

    df_copy = pd.concat(df_list)
    df_copy.pop('main')
    print(df_copy)
    # i dati vengono salvati in file diversi, per evitare che un errore possa fermare tutta l'acquisizione
    # con 16 thread ci vogliono quasi 6 ore per ottenere tutti i dati
    df_copy.to_csv(f'imdb_{start}_{end}.csv', index=None, header=True)

    if end == 2000:
        increment = 5
    if end == 2010:
        increment = 2
    start = end
    end += increment

