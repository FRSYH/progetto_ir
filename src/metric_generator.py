import pandas as pd
import numpy as np
from ast import literal_eval
import sklearn as sk
from sklearn.decomposition import PCA, IncrementalPCA
import statistics

'''
metric_generator.py
questo file legge il dataset grezzo ed applica una o piu specifiche metriche ai dati
le feature complesse, rappresentate come liste, vengono rimosse e rimpiazzate dalle metriche
'''


def create_one_hot_encoded_df(origin_df, one_hot_df, feature):
    for index, row in origin_df.iterrows():
        for item in row[feature]:
            one_hot_df.at[index, item] = 1.0


def mean(feature_dict, item, film_feature):
    sum = 0
    filmography = feature_dict[item]
    for film in filmography['films']:
        sum += film[film_feature]
    if sum == 0:
        return 0
    else:
        return sum / filmography['num_films']


def rating(feature_dict, item):
    return mean(feature_dict, item, 'rating')


def votes(feature_dict, item):
    sum = 0
    filmography = feature_dict[item]
    for film in filmography['films']:
        sum += film['votes']
    return sum


def increment_hindex(hindex, film, index_feature):
    value = film[index_feature]
    for index in hindex:
        if index_feature == 'rating':
            if value >= index:
                hindex[index] += 1
        else:
            if value >= index * 10:
                hindex[index] += 1
    return hindex


def hindex(feature_dict, item, index_feature):
    filmograpyh = feature_dict[item]
    films = filmograpyh['films']
    if not films:
        return 0
    else:
        for film in films:
            film['rating'] = round(film['rating'])
        hindex = {i: 0 for i in range(1, 11)}
        for film in films:
            hindex = increment_hindex(hindex, film, index_feature)
        for key, value in hindex.items():
            if value < key:
                if key - 1 >= 0:
                    return key - 1
                else:
                    return 0
        return 10


def simple_metric(df, metric_type):
    df.pop('index')
    df.pop('Budget')
    df.pop('Cumulative Worldwide Gross')
    df = df.dropna()
    df = df.reset_index(drop=True)
    df[['directors', 'cast', 'production companies']] = df[
        ['directors', 'cast', 'production companies']].applymap(
        literal_eval)

    for index, row in df.iterrows():
        for genre in row['genres'].split(','):
            df.at[index, genre] = 1
    df = df.fillna(0)
    categorical_feature = ['directors', 'production companies', 'cast']
    for feature in categorical_feature:
        print(feature)
        feature_col = df[feature]
        feature_list = feature_col.tolist()
        feature_list_flat = [item for sublist in feature_list for item in sublist]
        feature_set = set(feature_list_flat)
        print(len(feature_set))
        feature_dict = {i: {'num_films': 0, 'films': []} for i in feature_set}
        metric_dict = {}
        for index, row in df.iterrows():
            for item in row[feature]:
                feature_dict[item]['num_films'] += 1
                feature_dict[item]['films'].append({'film_id': row['primary_title'], 'rating': row['rating'],
                                                    'votes': row['votes']})
        feature_metric = 0
        for index, row in df.iterrows():
            for item in row[feature]:
                if metric_type == 'rating':
                    feature_metric += rating(feature_dict, item)
                elif metric_type == 'hindex':
                    feature_metric += hindex(feature_dict, item, rating)
                elif metric_type == 'votes':
                    feature_metric += votes(feature_dict, item)
            df.at[index, feature] = feature_metric
            feature_metric = 0
    return df


def mean_most_voted(feature_dict, item):
    sum = 0
    num_film = 0
    filmography = feature_dict[item]
    films = filmography['films']
    ordered_films = sorted(films, key=lambda k: k['votes'], reverse=True)
    votes_list = [x['votes'] for x in ordered_films if x]
    median = statistics.median(votes_list)
    for film in ordered_films:
        if film['votes'] >= median:
            sum += film['rating']
            num_film += 1
        else:
            break
    if sum == 0:
        return 0
    else:
        return sum / num_film


def complex_metric(df, metric_type):
    df.pop('index')
    df.pop('Budget')
    df.pop('Cumulative Worldwide Gross')
    df = df.dropna()
    df = df.reset_index(drop=True)
    df[['directors', 'cast', 'production companies']] = df[
        ['directors', 'cast', 'production companies']].applymap(
        literal_eval)

    for index, row in df.iterrows():
        for genre in row['genres'].split(','):
            df.at[index, genre] = 1
    df = df.fillna(0)
    categorical_feature = ['directors', 'production companies', 'cast']
    for feature in categorical_feature:
        print(feature)
        feature_col = df[feature]
        feature_list = feature_col.tolist()
        feature_list_flat = [item for sublist in feature_list for item in sublist]
        feature_set = set(feature_list_flat)
        print(len(feature_set))
        feature_dict = {i: {'num_films': 0, 'films': []} for i in feature_set}
        metric_dict = {}
        for index, row in df.iterrows():
            for item in row[feature]:
                feature_dict[item]['num_films'] += 1
                feature_dict[item]['films'].append({'film_id': row['primary_title'], 'rating': row['rating'],
                                                    'votes': row['votes']})

        # applicazione della metrica hindex qualita (hindex generato con i rating dei film)
        for index, row in df.iterrows():
            hindexs = {}
            for item in row[feature]:
                value = hindex(feature_dict, item, 'rating')
                if value not in hindexs:
                    hindexs[value] = [item]
                else:
                    hindexs[value].append(item)
            representative_items = hindexs[max(hindexs)]
            feature_metric = 0
            for item in representative_items:
                if metric_type == 'hindex_rating':
                    feature_metric += mean(feature_dict, item, 'rating')
                else:
                    feature_metric += mean_most_voted(feature_dict, item)
            feature_metric = feature_metric / len(representative_items)
            df.at[index, feature] = feature_metric

            # applicazione della metrica hindex popolarita (hindex generato con i voti dei film)
            hindexs = {}
            for item in row[feature]:
                value = hindex(feature_dict, item, 'votes')
                if value not in hindexs:
                    hindexs[value] = [item]
                else:
                    hindexs[value].append(item)
            representative_items = hindexs[max(hindexs)]
            feature_metric = 0
            for item in representative_items:
                if metric_type == 'hindex_rating':
                    feature_metric += mean(feature_dict, item, 'rating')
                else:
                    feature_metric += mean_most_voted(feature_dict, item)
            feature_metric = feature_metric / len(representative_items)
            # dato che viene applicata più di una metrica la secondo non può prendere il posto delle feature base
            # c'è bisogno di nuove colonne nel df originale
            title = feature + ' popolarity'
            df.at[index, title] = feature_metric
    return df


def metric(df, metric_type):
    if metric_type == 'hindex_rating' or metric_type == 'hindex_rating_votes':
        return complex_metric(df, metric_type)
    else:
        return simple_metric(df, metric_type)

# one hot ha grosse problematiche di memoria
# ci sono 1.5 milioni di attori, fare one-hot sul dataset completo è impossibile
def one_hot(original_df):
    original_df.pop('primary_title')
    original_df.pop('title_id')
    original_df.pop('index')
    original_df.pop('Budget')
    original_df.pop('Cumulative Worldwide Gross')
    original_df = original_df.dropna()
    original_df = original_df.reset_index(drop=True)
    original_df[['directors', 'cast', 'production companies']] = original_df[
        ['directors', 'cast', 'production companies']].applymap(
        literal_eval)

    original_df = original_df.head(1000)
    categorical_feature = ['directors', 'production companies', 'cast']
    feature_col = original_df['directors']
    feature_list = feature_col.tolist()
    feature_list_flat = [item for sublist in feature_list for item in sublist]
    feature_set = set(feature_list_flat)
    print(len(feature_set))

    one_hot = pd.DataFrame(0.0, index=original_df.index, columns=feature_set, dtype=np.int8)
    create_one_hot_encoded_df(original_df, one_hot, 'directors')
    return one_hot

# pca può ridurre la dimensionalità di one-hot ma per applicare pca è necessario farlo a blocchi
# ipca.partial_fit permette di applicare pca iterativamente
# pca risulta inutile, nessuna informazione, come del resto one-hot encode
def pca(original_df):
    original_df.pop('primary_title')
    original_df.pop('title_id')
    original_df.pop('index')
    original_df.pop('Budget')
    original_df.pop('Cumulative Worldwide Gross')
    original_df = original_df.dropna()
    original_df = original_df.reset_index(drop=True)
    original_df[['directors', 'cast', 'production companies']] = original_df[
        ['directors', 'cast', 'production companies']].applymap(
        literal_eval)

    n_components = 1
    num_chunks = 10
    categorical_feature = ['directors', 'production companies', 'cast']

    for feature in categorical_feature:
        if feature == 'cast':
            num_chunks *= 5
        print(original_df)
        print(feature)
        feature_col = original_df[feature]
        feature_list = feature_col.tolist()
        feature_list_flat = [item for sublist in feature_list for item in sublist]
        feature_set = set(feature_list_flat)
        print(len(feature_set))

        original_copy = original_df.copy()
        chunks = np.array_split(original_copy, num_chunks)
        result_chunks = np.array_split(original_copy, num_chunks)
        ipca = IncrementalPCA(n_components=n_components)

        for iteration, chunk in enumerate(chunks):
            print(f'fit chunck {iteration}')
            fit_df = pd.DataFrame(0.0, index=chunk.index, columns=feature_set)
            create_one_hot_encoded_df(chunk, fit_df, feature)
            ipca.partial_fit(fit_df)
            del fit_df

        for iteration, chunk in enumerate(chunks):
            print(f'transform chunk {iteration}')
            transform_df = pd.DataFrame(0.0, index=chunk.index, columns=feature_set)
            create_one_hot_encoded_df(chunk, transform_df, feature)

            ipca_df = ipca.transform(transform_df)
            del transform_df
            pca_features = []
            for num in range(1, (n_components + 1)):
                pca_features.append(f'{feature}_pca_{num}')

            ipca_df = pd.DataFrame(ipca_df, index=chunk.index, columns=pca_features)
            result_chunks[iteration].pop(feature)
            result_chunks[iteration] = result_chunks[iteration].join(ipca_df)
        original_df = pd.concat(result_chunks)
    return original_df


type = 'hindex_rating'
dataset = pd.read_csv('imdb_complete_dataset.csv')
print(dataset.dtypes)
if type == 'pca':
    dataset = pca(dataset)
else:
    dataset = metric(dataset, type)

print(dataset)
print('writing dataset')
dataset.to_csv(f'test_imdb_{type}.csv', index=True, header=True)
print('end writing')
