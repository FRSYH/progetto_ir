import pandas as pd
import numpy as np

'''
dataset_unifier.oy
riunisce i singoli dataset delle varie annate in un singolo file
'''

start = 1960
end = 1970
increment = 10
df_list = []
while start < 2020:
    file_name = f'imdb_{start}_{end}.csv'
    print(file_name)
    df = pd.read_csv(file_name)
    print(df)
    df_list.append(df)
    if end == 2000:
        increment = 5
    if end == 2010:
        increment = 2
    start = end
    end += increment
total_df = pd.concat(df_list)
total_df.to_csv('imdb_complete_dataset.csv', index=None, header=True)
